DROP TABLE Person;
DROP TABLE Occupation;
DROP TABLE Home;

CREATE TABLE Home(
hid char(4) PRIMARY KEY,
address varchar2(200) NOT NULL
);

CREATE TABLE Occupation(
oid char(4) PRIMARY KEY,
type varchar(20) NOT NULL,
salary number(10,2) NOT NULL
);

CREATE TABLE Person(
pid char(4) PRIMARY KEY,
firstname varchar2(20) NOT NULL,
lastname  varchar2(20) NOT NULL,
father_id char(4) REFERENCES Person(pid),
mother_id char(4) REFERENCES Person(pid),
hid char(4) REFERENCES Home(hid) NOT NULL,
oid char(4)  REFERENCES Occupation(oid) NOT NULL
);

INSERT INTO Home VALUES('H001', '123 Easy St.');
INSERT INTO Home VALUES('H002', '56 Fake Ln.');

INSERT INTO Occupation VALUES('O000', 'N/A', 0);
INSERT INTO Occupation VALUES('O001', 'Student', 0);
INSERT INTO Occupation VALUES('O002', 'Doctor', 100000);
INSERT INTO Occupation VALUES('O003', 'Professor', 80000);

INSERT INTO Person VALUES('P1', 'Zachary','Aberny',null,null,'H002','O000');
INSERT INTO Person VALUES('P2', 'Yanni','Aberny',null,null,'H002','O000');
INSERT INTO Person VALUES('P3', 'Alice','Aberny','P1','P2', 'H001','O002');
INSERT INTO Person VALUES('P4', 'Bob', 'Bortelson',null,null,'H001','O003');
INSERT INTO Person VALUES('P5', 'Carl','Aberny-Bortelson','P4','P3','H001','O001');
INSERT INTO Person VALUES('P6', 'Denise','Aberny-Bortelson','P4','P3','H001','O001');

--Test Query
SELECT gp.firstname FROM person gp 
JOIN person p ON gp.pid = p.mother_id OR gp.pid = p.father_id JOIN
person c ON p.pid = c.mother_id OR p.pid = c.father_id WHERE c.firstname = 'Denise';

--1
SELECT hid,address,COUNT(address) AS "Number of people" FROM Home
JOIN PERSON USING(hid)
GROUP BY hid,address;

--2
SELECT DISTINCT p3.firstname,p3.lastname FROM Person p1
INNER JOIN Person p2 ON p1.father_id= p2.pid OR p1.mother_id=p2.pid
INNER JOIN Person p3 ON p2.father_id=p3.pid;

--3
SELECT DISTINCT p1.firstname,p1.lastname FROM Person p1
INNER JOIN Person p2 ON p1.father_id=p2.pid OR p1.mother_id=p2.pid
INNER JOIN Occupation o ON p1.oid=o.oid
INNER JOIN Home h ON p1.hid=h.hid
WHERE p1.oid='O001' AND p1.hid=p2.hid;
--4
SELECT MAX(SUM(o.salary)) AS "Max Household Salary" FROM PERSON p
INNER JOIN Occupation o ON o.oid=p.oid
GROUP BY p.hid;

