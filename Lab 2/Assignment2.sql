CREATE OR REPLACE PACKAGE book_store AS
   FUNCTION get_price_after_tax(book_isbn varchar2)
   RETURN number;
END book_store;
/

CREATE OR REPLACE PACKAGE BODY book_store AS
    FUNCTION price_after_discount(book_isbn varchar2)
        RETURN number
        IS price_after number;
    BEGIN
        SELECT RETAIL-DISCOUNT INTO price_after FROM BOOKS
        WHERE isbn=book_isbn;
        RETURN price_after;
    END;
    
   FUNCTION get_price_after_tax(book_isbn varchar2)
       RETURN number
       AS final_price number;
   BEGIN
        final_price :=price_after_discount(book_isbn);
        RETURN final_price*0.15+final_price;
   END;
END book_store;
/

DECLARE
    book1_final_price number;
    book2_final_price number;
    book_isbn1 varchar2(50);
    book_isbn2 varchar2(50);
BEGIN
    SELECT isbn into book_isbn1 FROM BOOKS
    WHERE title='BUILDING A CAR WITH TOOTHPICKS';
    
    SELECT isbn into book_isbn2 FROM BOOKS
    WHERE title='HOLY GRAIL OF ORACLE';
    
    book1_final_price:=book_store.get_price_after_tax(book_isbn1);
    book2_final_price:=book_store.get_price_after_tax(book_isbn2);
    dbms_output.put_line('BUILDING A CAR WITH TOOTHPICKS price ' || book1_final_price||'$');
    dbms_output.put_line('HOLY GRAIL OF ORACLE price ' || book2_final_price||'$');
END;
/