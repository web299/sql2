
CREATE OR REPLACE PACKAGE book_store AS
   TYPE customerIdArrayType IS VARRAY(100) OF varchar2(100);
   
   /*FUNCTION get_price_after_tax(book_isbn varchar2)
   RETURN number;*/
   
   PROCEDURE show_purchases;
   
   PROCEDURE add_book(book_isbn IN books.isbn%TYPE, book_title IN books.title%TYPE, book_pubdate IN books.pubdate%TYPE,book_pubid IN books.pubid%TYPE,book_cost IN books.cost%TYPE,book_retail IN books.retail%TYPE,book_discount IN books.discount%TYPE,book_category IN books.category%TYPE);

END book_store;
/

CREATE OR REPLACE PACKAGE BODY book_store AS

    /*FUNCTION price_after_discount(book_isbn varchar2)
    RETURN number
        IS price_after number;
    BEGIN
        SELECT RETAIL-DISCOUNT INTO price_after FROM BOOKS
        WHERE isbn=book_isbn;
        RETURN price_after;
    END;*/
    
   /*FUNCTION get_price_after_tax(book_isbn varchar2)
   RETURN number
       AS final_price number;
   BEGIN
        final_price :=price_after_discount(book_isbn);
        RETURN final_price*0.15+final_price;
   END;*/
   
   FUNCTION book_purchasers(book_isbn books.isbn%TYPE)
   RETURN customerIdArrayType
   AS
       idHolder customerIdArrayType;
    BEGIN
        SELECT DISTINCT customer# bulk collect into idHolder FROM BOOKS
        INNER JOIN ORDERITEMS USING(isbn)
        INNER JOIN ORDERS USING(order#)
        INNER JOIN CUSTOMERS USING(customer#)
        WHERE book_isbn=isbn;
        RETURN idHolder;
    END;
    
    PROCEDURE show_purchases
    AS
        buyers customerIdArrayType;
        name varchar2(100);
        fullname varchar2(500);
    BEGIN
        fullname:='';
        FOR book_row IN (SELECT * FROM BOOKS)
        LOOP
            buyers:=book_purchasers(book_row.isbn);
            FOR counter IN 1 .. buyers.COUNT LOOP
                SELECT  customers.firstname||' '||customers.lastname||' , ' INTO name FROM CUSTOMERS
                WHERE customer#=buyers(counter);
                fullname:=fullname||name;
            END LOOP;
            dbms_output.put_line(book_row.isbn||' : '||book_row.title||' : '||fullname);
            fullname:='';
        END LOOP;
    END;
    
    PROCEDURE add_book(book_isbn IN books.isbn%TYPE, book_title IN books.title%TYPE, book_pubdate IN books.pubdate%TYPE,book_pubid IN books.pubid%TYPE,book_cost IN books.cost%TYPE,book_retail IN books.retail%TYPE,book_discount IN books.discount%TYPE,book_category IN books.category%TYPE)
    AS
        countIsbn number;
        invalid number;
    BEGIN
        invalid:=0;
        SELECT COUNT(books.isbn) INTO countIsbn FROM books;
        FOR cursorIsbn IN(SELECT * FROM books) LOOP
            IF book_isbn!=cursorIsbn.isbn
            THEN
                invalid:=invalid+1;
                IF invalid=countIsbn THEN
                    INSERT INTO books VALUES(book_isbn,book_title,book_pubdate,book_pubid,book_cost,book_retail,book_discount,book_category);
                    EXIT;
                END IF;
             ELSE
                IF book_title!=cursorIsbn.title THEN
                        UPDATE books SET title=book_title WHERE book_isbn=isbn;
                END IF;
                IF book_pubdate!=cursorIsbn.pubdate THEN
                        UPDATE books SET pubdate=book_pubdate WHERE book_isbn=isbn;
                END IF;
                IF book_pubid!=cursorIsbn.pubid THEN
                        UPDATE books SET pubid=book_pubid WHERE book_isbn=isbn;
                END IF;
                IF book_cost!=cursorIsbn.cost THEN
                        UPDATE books SET cost=book_cost WHERE book_isbn = isbn;
                END IF;
                IF book_retail!=cursorIsbn.retail THEN
                        UPDATE books SET retail=book_retail WHERE book_isbn=isbn;
                END IF;
                IF book_discount!=cursorIsbn.discount THEN
                        UPDATE books SET discount=book_discount WHERE book_isbn=isbn;
                END IF;
                IF book_category!=cursorIsbn.category THEN
                        UPDATE books SET category=book_category WHERE book_isbn=isbn;
                END IF;
            END IF;
        END LOOP;
    END;
END book_store;
/

DECLARE

BEGIN
    book_store.show_purchases;
    book_store.add_book('1382200318','WE GO JIM','10-SEP-22',3,100,145,12,'FITNESS');
    book_store.add_book('0401140733','REVENGE OF MICKEY','14-DEC-05',1,99.99,22,NULL,'FAMILY LIFE');
    --ROLLBACK;
END;
/

