DROP TABLE dawson_courses;

DROP TABLE term_seasons;

DROP TABLE seasons;

DROP TABLE educations;
----------------------------------
DROP TYPE course_type;

DROP TYPE education_type;

DROP TYPE term_type;

DROP TYPE season_type;

--CREATING TABLES
CREATE TABLE seasons (
    season_id   CHAR(1) PRIMARY KEY,
    season_name VARCHAR2(20) NOT NULL
);

CREATE TABLE term_seasons (
    term_id   NUMBER(2, 0) PRIMARY KEY,
    season_id CHAR(1)
        REFERENCES seasons ( season_id )
);

CREATE TABLE educations (
    education_type_id CHAR(1) PRIMARY KEY,
    education_type    VARCHAR2(30) NOT NULL
);

CREATE TABLE dawson_courses (
    course_number      VARCHAR2(20) PRIMARY KEY,
    course_name        VARCHAR2(50) NOT NULL,
    course_description VARCHAR2(1000) NOT NULL,
    class_hours        NUMBER(2, 0),
    lab_hours          NUMBER(2, 0),
    homework_hours     NUMBER(2, 0),
    education_type_id  CHAR(1)
        REFERENCES educations ( education_type_id ),
    term_id            NUMBER(2, 0)
        REFERENCES term_seasons ( term_id )
);

INSERT INTO seasons VALUES (
    '1',
    'Fall'
);

INSERT INTO seasons VALUES (
    '2',
    'Winter'
);

INSERT INTO educations VALUES (
    '1',
    'General'
);

INSERT INTO educations VALUES (
    '2',
    'Concentration'
);

INSERT INTO term_seasons VALUES (
    1,
    '1'
);

INSERT INTO term_seasons VALUES (
    2,
    '2'
);

INSERT INTO dawson_courses VALUES (
    '420-110-DW',
    'Programming I',
    'The course will introduce the student to the basic building blocks (sequential,
selection and repetitive control structures) and modules (methods and classes)
used to write a program. The student will use the Java programming language to
implement the algorithms studied. The array data structure is introduced, and
student will learn how to program with objects.',
    3,
    3,
    3,
    '2',
    1
);

INSERT INTO dawson_courses VALUES (
    '420-210-DW',
    'Programming II',
    'The course will introduce the student to basic object-oriented methodology in
order to design, implement, use and modify classes, to write programs in the
Java language that perform interactive processing, array and string processing,
and data validation. Object-oriented features such as encapsulation and
inheritance will be explored.',
    3,
    3,
    3,
    '2',
    2
);

COMMIT;
-------------------------------Code for Assignment 8 starts here-------------------------------------------------

CREATE OR REPLACE TYPE season_type AS OBJECT (
    season_id   CHAR(1),
    season_name VARCHAR2(20)
);
/

CREATE OR REPLACE TYPE term_type AS OBJECT (
    term_id NUMBER(2, 0),
    vseason season_type
);
/

CREATE OR REPLACE TYPE education_type AS OBJECT (
    education_type_id CHAR(1),
    education_type    VARCHAR2(30)
);
/

CREATE OR REPLACE TYPE course_type AS OBJECT (
    course_number      VARCHAR2(20),
    course_name        VARCHAR2(50),
    course_description VARCHAR2(1000),
    class_hours        NUMBER(2, 0),
    lab_hours          NUMBER(2, 0),
    homework_hours     NUMBER(2, 0),
    veducation         education_type,
    vterm              term_type
);
/

CREATE OR REPLACE PACKAGE dawson_classes AS
    PROCEDURE add_season (
        vseason IN season_type
    );

    PROCEDURE add_term (
        vterm IN term_type
    );

    PROCEDURE add_education (
        veducation IN education_type
    );

    PROCEDURE add_course (
        vcourse IN course_type
    );

END dawson_classes;
/

CREATE OR REPLACE PACKAGE BODY dawson_classes AS

    PROCEDURE add_season (
        vseason IN season_type
    ) AS
    BEGIN
        INSERT INTO seasons VALUES (
            vseason.season_id,
            vseason.season_name
        );
    END;
   
    PROCEDURE add_term (
        vterm IN term_type
    ) AS
    BEGIN
        INSERT INTO term_seasons VALUES (
            vterm.term_id,
            vterm.vseason.season_id
        );

    END;
    
    PROCEDURE add_education (
        veducation IN education_type
    ) AS
    BEGIN
        INSERT INTO educations VALUES (
            veducation.education_type_id,
            veducation.education_type
        );

    END;
    
    PROCEDURE add_course (
        vcourse IN course_type
    ) AS
    BEGIN
        INSERT INTO dawson_courses VALUES (
            vcourse.course_number,
            vcourse.course_name,
            vcourse.course_description,
            vcourse.class_hours,
            vcourse.lab_hours,
            vcourse.homework_hours,
            vcourse.veducation.education_type_id,
            vcourse.vterm.term_id
        );

    END;
    
END dawson_classes;
/
COMMIT;

