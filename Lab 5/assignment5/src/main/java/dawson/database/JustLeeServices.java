package dawson.database;

import java.sql.*;

public class JustLeeServices {
    public static void main(String[] args) {
        try (Connection conn = getConnection()) {
            Book newBook = new Book("0123456789", "GOD OF SQL", Date.valueOf("2022-09-22"), getPublisher(conn, 5),
                    40.20, 82.99, 3.2,
                    "COMPUTER");
            addBook(conn, newBook);
            System.out.println(getBook(conn, newBook.getIsbn()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private static Connection getConnection() throws SQLException {
        String user = System.console().readLine("Username: ");
        String password = new String(System.console().readPassword("Password: "));
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        Connection conn = DriverManager.getConnection(url,
                user, password);
        return conn;
    }

    private static Book getBook(Connection conn, String isbn) {
        Book book = null;
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM books WHERE isbn=?")) {
            stmt.setString(1, isbn);
            ResultSet result = stmt.executeQuery();
            while (result.next()) {
                book = new Book(isbn, result.getString("title"), result.getDate("pubdate"),
                        getPublisher(conn, result.getInt("pubid")), result.getDouble("cost"),
                        result.getDouble("retail"),
                        result.getDouble("discount"), result.getString("category"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return book;
    }

    private static Publisher getPublisher(Connection conn, int pub_id) {
        Publisher publisher = null;
        try (PreparedStatement statement = conn.prepareStatement("SELECT * FROM publisher WHERE pubid=?")) {
            statement.setInt(1, pub_id);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                publisher = new Publisher(pub_id, resultSet.getString("name"), resultSet.getString("contact"),
                        resultSet.getString("phone"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return publisher;
    }

    private static void addBook(Connection conn, Book book) {
        String sql = "INSERT INTO books VALUES(?,?,?,?,?,?,?,?)";
        try (PreparedStatement statement = conn.prepareStatement(sql)) {
            statement.setString(1, book.getIsbn());
            statement.setString(2, book.getTitle());
            statement.setDate(3, book.getPubDate());
            statement.setInt(4, book.getPublisher().getPubId());
            statement.setDouble(5, book.getCost());
            statement.setDouble(6, book.getRetail());
            statement.setDouble(7, book.getDiscount());
            statement.setString(8, book.getCategory());
            int result=statement.executeUpdate();
        } catch (NullPointerException e) {
            System.out.println("Publisher with the given pubid does not exist");
        }
        catch(SQLIntegrityConstraintViolationException e){
            System.out.println("Book with the given isbn already exists");
        }
        catch(SQLException e){
            if(e.getErrorCode()==12899){
                System.out.println("One of the String inputs has more charachters than the one idicated in the database.");
            }
        }
    }
}