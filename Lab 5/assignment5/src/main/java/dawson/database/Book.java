package dawson.database;

import java.sql.Date;

public class Book {
    private String isbn;
    private String title;
    private Date pubDate;
    private Publisher publisher;
    private double cost;
    private double retail;
    private double discount;
    private String category;

    public Book(String isbn, String title, Date pubDate, Publisher publisher, double cost, double retail,
            double discount, String category) {
        if(isbn==null){
            throw new IllegalArgumentException("isbn cannot be null");
        }
        this.isbn = isbn;
        this.title = title;
        this.pubDate = pubDate;
        this.publisher = publisher;
        this.cost = cost;
        this.retail = retail;
        this.discount = discount;
        this.category = category;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getPubDate() {
        return pubDate;
    }

    public void setPubDate(Date pubDate) {
        this.pubDate = pubDate;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getRetail() {
        return retail;
    }

    public void setRetail(double retail) {
        this.retail = retail;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {
        return "Book [category=" + category + ", cost=" + cost + ", discount=" + discount + ", isbn=" + isbn
                + ", pubDate=" + pubDate + ", publisher=" + publisher + ", retail=" + retail + ", title=" + title + "]";
    }

}
