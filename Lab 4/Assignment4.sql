
CREATE OR REPLACE PACKAGE book_store AS
   --TYPE customerIdArrayType IS VARRAY(100) OF varchar2(100);
   
   /*FUNCTION get_price_after_tax(book_isbn varchar2)
   RETURN number;*/
   
   /*PROCEDURE show_purchases;
   
   PROCEDURE add_book(book_isbn IN books.isbn%TYPE, book_title IN books.title%TYPE, book_pubdate IN books.pubdate%TYPE,book_pubid IN books.pubid%TYPE,book_cost IN books.cost%TYPE,book_retail IN books.retail%TYPE,book_discount IN books.discount%TYPE,book_category IN books.category%TYPE);*/

   PROCEDURE rename_category(category_name IN books.category%TYPE,change_category IN books.category%TYPE);
   
   category_not_found EXCEPTION;
   
   publisher_already_exists EXCEPTION;
   
   PROCEDURE add_publisher(pub_name publisher.name%TYPE,pub_contact publisher.contact%TYPE,pub_phone publisher.phone%TYPE);
    
END book_store;
/

CREATE OR REPLACE PACKAGE BODY book_store AS

    /*FUNCTION price_after_discount(book_isbn varchar2)
    RETURN number
        IS price_after number;
    BEGIN
        SELECT RETAIL-DISCOUNT INTO price_after FROM BOOKS
        WHERE isbn=book_isbn;
        RETURN price_after;
    END;*/
    
   /*FUNCTION get_price_after_tax(book_isbn varchar2)
   RETURN number
       AS final_price number;
   BEGIN
        final_price :=price_after_discount(book_isbn);
        RETURN final_price*0.15+final_price;
   END;*/
   
   /*FUNCTION book_purchasers(book_isbn books.isbn%TYPE)
   RETURN customerIdArrayType
   AS
       idHolder customerIdArrayType;
    BEGIN
        SELECT DISTINCT customer# bulk collect into idHolder FROM BOOKS
        INNER JOIN ORDERITEMS USING(isbn)
        INNER JOIN ORDERS USING(order#)
        INNER JOIN CUSTOMERS USING(customer#)
        WHERE book_isbn=isbn;
        RETURN idHolder;
    END;
    
    PROCEDURE show_purchases
    AS
        buyers customerIdArrayType;
        name varchar2(100);
        fullname varchar2(500);
    BEGIN
        fullname:='';
        FOR book_row IN (SELECT * FROM BOOKS)
        LOOP
            buyers:=book_purchasers(book_row.isbn);
            FOR counter IN 1 .. buyers.COUNT LOOP
                SELECT  customers.firstname||' '||customers.lastname||' , ' INTO name FROM CUSTOMERS
                WHERE customer#=buyers(counter);
                fullname:=fullname||name;
            END LOOP;
            dbms_output.put_line(book_row.isbn||' : '||book_row.title||' : '||fullname);
            fullname:='';
        END LOOP;
    END;*/
    
    /*PROCEDURE add_book(book_isbn IN books.isbn%TYPE, book_title IN books.title%TYPE, book_pubdate IN books.pubdate%TYPE,book_pubid IN books.pubid%TYPE,book_cost IN books.cost%TYPE,book_retail IN books.retail%TYPE,book_discount IN books.discount%TYPE,book_category IN books.category%TYPE)
    AS
        countIsbn number;
        invalid number;
    BEGIN
        invalid:=0;
        SELECT COUNT(books.isbn) INTO countIsbn FROM books;
        FOR cursorIsbn IN(SELECT * FROM books) LOOP
            IF book_isbn!=cursorIsbn.isbn
            THEN
                invalid:=invalid+1;
                IF invalid=countIsbn THEN
                    INSERT INTO books VALUES(book_isbn,book_title,book_pubdate,book_pubid,book_cost,book_retail,book_discount,book_category);
                    EXIT;
                END IF;
             ELSE
                IF book_title!=cursorIsbn.title THEN
                        UPDATE books SET title=book_title WHERE book_isbn=isbn;
                END IF;
                IF book_pubdate!=cursorIsbn.pubdate THEN
                        UPDATE books SET pubdate=book_pubdate WHERE book_isbn=isbn;
                END IF;
                IF book_pubid!=cursorIsbn.pubid THEN
                        UPDATE books SET pubid=book_pubid WHERE book_isbn=isbn;
                END IF;
                IF book_cost!=cursorIsbn.cost THEN
                        UPDATE books SET cost=book_cost WHERE book_isbn = isbn;
                END IF;
                IF book_retail!=cursorIsbn.retail THEN
                        UPDATE books SET retail=book_retail WHERE book_isbn=isbn;
                END IF;
                IF book_discount!=cursorIsbn.discount THEN
                        UPDATE books SET discount=book_discount WHERE book_isbn=isbn;
                END IF;
                IF book_category!=cursorIsbn.category THEN
                        UPDATE books SET category=book_category WHERE book_isbn=isbn;
                END IF;
            END IF;
        END LOOP;
    END;*/
    
    PROCEDURE rename_category(category_name IN books.category%TYPE,change_category IN books.category%TYPE)
    AS
    BEGIN
        UPDATE books SET category=change_category WHERE category=category_name;
        IF SQL%NOTFOUND THEN
            RAISE book_store.category_not_found;
        END IF; 
    END;
    
    PROCEDURE add_publisher(pub_name publisher.name%TYPE,pub_contact publisher.contact%TYPE,pub_phone publisher.phone%TYPE)
    AS
        pub_pubid publisher.pubid%TYPE;
    BEGIN
         FOR a_row IN (SELECT name FROM publisher) LOOP
            IF a_row.name=pub_name THEN
                RAISE book_store.publisher_already_exists;
            END IF;
        END LOOP;
        SELECT MAX(pubid)+1 INTO pub_pubid FROM publisher;
        INSERT INTO publisher VALUES(pub_pubid,pub_name,pub_contact,pub_phone);
    END;
END book_store;
/

DECLARE
    
BEGIN
    /*book_store.show_purchases;
    book_store.add_book('1382200318','WE GO JIM','10-SEP-22',3,100,145,12,'FITNESS');
    book_store.add_book('0401140733','REVENGE OF MICKEY','14-DEC-05',1,99.99,22,NULL,'FAMILY LIFE');*/
    BEGIN
        book_store.rename_category('COMPUTER','COMPUTER SCIENCE');
    EXCEPTION
        WHEN book_store.category_not_found THEN
        dbms_output.put_line('No category was found.');
        WHEN OTHERS THEN
        IF SQLCODE = -12899 THEN
             dbms_output.put_line('The new category length is too large for the varchar2 limit of the column.');
        END IF;
    END;
    
    BEGIN
        book_store.rename_category('TEACHING','EDUCATION');
    EXCEPTION
        WHEN book_store.category_not_found THEN
        dbms_output.put_line('No category was found.');
        WHEN OTHERS THEN
        IF SQLCODE = -12899 THEN
             dbms_output.put_line('The new category length is too large for the varchar2 limit of the column.');
        END IF;
    END;
    
    BEGIN
        book_store.add_publisher('DAWSON PRINTING','JOHN SMITH','111-555-2233');
    EXCEPTION
        WHEN book_store.publisher_already_exists THEN
        dbms_output.put_line('The publisher already exists.');
    END;
    
    BEGIN
        book_store.add_publisher('PUBLISH OUR WAY','JANE TOMLIN','010-410-0010');
    EXCEPTION
        WHEN book_store.publisher_already_exists THEN
        dbms_output.put_line('The publisher already exists.');
    END;
    
    --ROLLBACK;
END;
/

SELECT * FROM publisher;

