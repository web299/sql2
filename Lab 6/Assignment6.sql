DROP INDEX my_index1;
DROP INDEX my_index2;
DROP INDEX my_index3;
DROP TABLE book_authors;
DROP TABLE authors;
DROP TABLE books;
DROP TABLE cities;
DROP TABLE countries;

CREATE TABLE books(
    isbn char(6) PRIMARY KEY,
    title varchar2(50) NOT NULL,
    category varchar2(50) NOT NULL,
    price number(5,2) NOT NULL
);

CREATE TABLE cities(
    city_id number(5) PRIMARY KEY,
    city varchar2(50) NOT NULL
);
CREATE TABLE countries(
    country_id number(3) PRIMARY KEY,
    country varchar(50) NOT NULL
);
CREATE TABLE authors(
    author_id number(6) PRIMARY KEY,
    author_name varchar2(50) NOT NULL,
    city_id number(5) REFERENCES cities(city_id),
    country_id number(3) REFERENCES countries(country_id)
);
CREATE TABLE book_authors(
    isbn char(6) REFERENCES books(isbn),
    author_id number(6) REFERENCES authors(author_id)
);

INSERT INTO books VALUES('123ABC','How to book', 'Business',20.23 );
INSERT INTO books VALUES('AJKAJA','Wow hobbits!', 'Fantasy',10.2 );
INSERT INTO books VALUES('AJSHAK','Science Adventure', 'Sci-fi',13.12 );
INSERT INTO books VALUES('OAISJD','It''s cooking', 'Cooking',35.36 );
INSERT INTO books VALUES('ASDOA','The times and ideas of Ronald McRonald', 'Biography',23 );
INSERT INTO books VALUES('JKLXCD','Can you really ever book?', 'Business',30.12 );
INSERT INTO books VALUES('ASDSAK','Geez its hobbits.', 'Fantasy',4.5 );

INSERT INTO cities VALUES(1,'Montreal');
INSERT INTO cities VALUES(2,'Birmingham');
INSERT INTO cities VALUES(3,'Toronto');

INSERT INTO countries VALUES(1,'Canada');
INSERT INTO countries VALUES(2,'England');

INSERT INTO authors VALUES(1,'Reginald Authorson',1,1);
INSERT INTO authors VALUES(2,'XYZ Tolkeen',2,2);
INSERT INTO authors VALUES(3,'B Ronalds',2,2);
INSERT INTO authors VALUES(4,'Janeet Paulton',3,1);
INSERT INTO authors VALUES(5,'Vanti Raulson',3,1);

INSERT INTO book_authors VALUES('123ABC',1);
INSERT INTO book_authors VALUES('AJKAJA',2);
INSERT INTO book_authors VALUES('AJSHAK',3);
INSERT INTO book_authors VALUES('OAISJD',4);
INSERT INTO book_authors VALUES('ASDOA',5);
INSERT INTO book_authors VALUES('JKLXCD',1);
INSERT INTO book_authors VALUES('ASDSAK',2);
INSERT INTO book_authors VALUES('ASDOA',4);

SELECT COUNT(title) AS "NUMBER OF BOOKS MORE EXPENSIVE THAN 15$" FROM books WHERE price>15;

SELECT title, author_name FROM books b
INNER JOIN book_authors ba ON b.isbn=ba.isbn
INNER JOIN authors au ON au.author_id=ba.author_id
WHERE category='Fantasy';

SELECT title, author_name FROM books b
INNER JOIN book_authors ba ON b.isbn=ba.isbn
INNER JOIN authors au ON au.author_id=ba.author_id
INNER JOIN countries c ON c.country_id=au.country_id
WHERE country='Canada';


CREATE OR REPLACE VIEW bookInfromation AS
SELECT title,author_name,price,city FROM books b
INNER JOIN book_authors ba ON b.isbn=ba.isbn
INNER JOIN authors au ON au.author_id=ba.author_id
INNER JOIN cities c ON c.city_id=au.city_id
WHERE category='Business';


SELECT * FROM bookInfromation;

CREATE INDEX my_index1 ON books(title,category);
CREATE INDEX my_index2 ON authors(author_name);
CREATE INDEX my_index3 ON cities(city);

 

