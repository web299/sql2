select * from books;
select * from books where cost > 20;
select sum(cost) from books where cost > 20;

drop table addresses;
create table addresses (
    id char(4) primary key,
    street_number varchar2(20) not null,
    street varchar2(100) not null,
    city varchar2(100) not null,
    province varchar2(100) not null,
    postal_code char(6) not null
);

insert into addresses values ('0000', '123', 
'Maple St', 'Montreal', 'Quebec', 'h3h1j1');

grant select on addresses to public;

select * from ddubois.addresses;

-- Shows all tables you can access
SELECT
  table_name, owner
FROM
  all_tables
ORDER BY
  owner, table_name;


select * from books where category = 'COMPUTER';
create index categroyindex on books(category);
drop index categroyindex;

create view title_author_view as 
select title, fname, lname, cost from books 
join bookauthor using (isbn) join author using (authorid);

select * from title_author_view;