package dawson.database;
import java.sql.*;

public class App 
{
    public static void main( String[] args ) throws SQLException
    {
        String user = System.console().readLine("Username: ");
        String password = new String(System.console().readPassword("Password: "));
        String url = "jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca";
        try(Connection conn = DriverManager.getConnection(url, 
        user, password)){
            System.out.println( conn.isClosed() );

            PreparedStatement statement = conn.prepareStatement("select * from hr.departments");
            ResultSet result = statement.executeQuery();
            while(result.next()){
                String deptName = result.getString("department_name");
                System.out.println(deptName);
            }
    
            Location loc = getLocation(conn, 1000);
            loc.setPostalCode("12345");
            updateLocation(conn, loc);
            Country canada = getCountry(conn, "CA");
            Location newLoc = new Location(2000, null,
             null, "Montreal", "QC", canada);
            addLocation(conn, newLoc);

            System.out.println(loc);
        }
        catch(SQLException e){
            e.printStackTrace();
        }
    }

    private static void addLocation(Connection conn, Location loc) {
        String sql = "insert into locations values (?,?,?,?,?,?)";
        try(PreparedStatement stmt = conn.prepareStatement(sql)){
            stmt.setInt(1, loc.getLocationId());
            stmt.setString(2, loc.getStreetAddress());
            stmt.setString(3, loc.getPostalCode());
            stmt.setString(4, loc.getCity());
            stmt.setString(5, loc.getStateProvince());
            stmt.setString(6, loc.getCountry().getCountryId());
            int result = stmt.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private static void updateLocation(Connection conn, Location loc) {
        String sql = "update locations set postal_code = ?, street_address = ?, city = ?, state_province= ?, country_id = ? where location_id = ?";
        try(PreparedStatement stmt = conn.prepareStatement(sql)){
            stmt.setString(1, loc.getPostalCode());
            stmt.setString(2, loc.getStreetAddress());
            stmt.setString(3, loc.getCity());
            stmt.setString(4, loc.getStateProvince());
            stmt.setString(5, loc.getCountry().getCountryId());
            stmt.setInt(6, loc.getLocationId());
            int result = stmt.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    private static Location getLocation(Connection conn, int locationId) throws SQLException{
        Location location = null;
        PreparedStatement stmt = null;
        try{
            stmt = conn.prepareStatement(
                "select * from hr.locations where location_id=?");
            stmt.setInt(1, locationId);
            ResultSet result = stmt.executeQuery();
            while(result.next()){

                location = new Location(locationId, 
                result.getString("street_address"), 
                result.getString("postal_code"), 
                result.getString("city"), 
                result.getString("state_province"), 
                getCountry(conn, result.getString("country_id")));
            }
            
        } catch(SQLException e){
            e.printStackTrace();
        } finally{
            if(stmt != null && !stmt.isClosed()){
                stmt.close();    
            }
        }
        
        return location;
    }

    private static Country getCountry(Connection conn, String countryId) {
        Country country = null;
        try(PreparedStatement stmt = conn.prepareStatement(
            "Select * from hr.countries where country_id = ?") ) {
                
            stmt.setString(1, countryId);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                country= new Country(countryId, 
                result.getString("country_name"), 
                result.getInt("region_id"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return country;
    }
}
