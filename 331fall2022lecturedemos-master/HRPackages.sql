create or replace package hr_package as
    function get_dept_id(dept_name varchar2) return number;
    function get_city(dept_name varchar2) return varchar2;
    function print_department_names return number;
    type dept_id_array_type is varray(200) of number;
    no_city exception;
end hr_package;
/
create or replace package body hr_package as
    function get_city_name(loc_id number) return varchar2 is
        city_name varchar2(200);
        begin
        select city into city_name from hr.locations where location_id=loc_id;
        return city_name;
        exception
        when no_data_found then
            raise no_city;
        end;
    function get_dept_id(dept_name varchar2) return number is
        dept_id number;
        begin
            select department_id into dept_id from HR.departments 
            where department_name = dept_name;
        return (dept_id);
        exception 
        when no_data_found then
            return -1;
        end;
    function get_city(dept_name varchar2) return varchar2 is
        city_name varchar2(200);
        dept_id number;
        loc_id number;
        begin
            dept_id := get_dept_id(dept_name);
            select location_id into loc_id from hr.departments 
            where department_id=dept_id;
            city_name := get_city_name(loc_id);
            return city_name;
        exception when no_data_found then
            raise no_city;
        end;
    function print_department_names return number is
        dept_count number;
    begin
        select count(department_id) into dept_count from hr.departments;
        for arow in (select department_id, department_name from hr.departments) loop
            dbms_output.put_line(arow.department_id || ' : ' || arow.department_name);
        end loop;
        return dept_count;
    end;
        
end hr_package;
/
declare
    dept_id number;
    city_name varchar2(200);
    dept_ids hr_package.dept_id_array_type;
    dept_count number;
begin
    dept_id := hr_package.get_dept_id('Marketing');
    city_name := hr_package.get_city('Marketing');
    dbms_output.put_line(dept_id);
    dbms_output.put_line(city_name);
    
    select department_id bulk collect into dept_ids from hr.departments;
    for i in 1 .. dept_ids.count loop
        dbms_output.put_line(dept_ids(i));
    end loop;
    
    dept_count := hr_package.print_department_names();
    dbms_output.put_line('Count: ' || dept_count);
    
    dept_id := hr_package.get_dept_id('Security');
    dbms_output.put_line('dept ID: ' || dept_id);
    
    city_name := hr_package.get_city('Marketings');
    dbms_output.put_line(city_name);
exception
    when hr_package.no_city then
        dbms_output.put_line('Couldn''t find city');
end;