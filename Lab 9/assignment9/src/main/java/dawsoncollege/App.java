package dawsoncollege;
import java.sql.SQLException;
import java.util.Scanner;
public class App {
    public static void main( String[] args ) throws SQLException{
        //If the user attempts to login to his account and fails, after five times, he can no longer try to login.
        int loginAttempt=0;
        Scanner reader=new Scanner(System.in);
        String username = System.console().readLine("Enter your Username for connecting to the database: ");
        String password = new String(System.console().readPassword("Enter your Password for connecting to the database: "));
        JLSecurity jlSecurity = new JLSecurity(username, password);
        //Acces is a variable that is true until the moment that the user has more than 5 failed login attempts. If the failed login attempts 
        //surpasses 5, the loging option in the menu gets disabled and a new prompt is displayed to the user.
        boolean access=true;

        boolean programRuns = true;
        while(programRuns){
            //Deciding on the prompt based on the value of access variable.
            if(access){
                System.out.println("Select 1 if you want to login, select 2 if you want to create a new account or select 3 to quit.");
            }else{
                System.out.println("Select 2 if you want to create a new account or select 3 to quit.");
            }
            int input=reader.nextInt();
            if(input==1 && access){
                while(loginAttempt<5){
                    if(login(jlSecurity)==false){
                        System.out.println("Invalid username or password! Try again: ");
                        System.out.println("Do you want to continue your attempt to login?\nType 1 to exit or something else to continue logining");
                        int decision=reader.nextInt();
                        if(decision==1){
                            break;
                        }
                        loginAttempt++;
                        if (loginAttempt%5==0) {
                            System.out.println("Your access is denied due to too many login attempts. Please try again later.");
                            access=false;
                            break;
                        }
                    }else{
                        System.out.println("Access granted");
                        loginAttempt=0;
                        break;
                    }
                }  
            }
            else if(input==2){
                CreateNewUser(jlSecurity);
            }
            else if(input==3){
                jlSecurity.closeConnection();
                break;
            }
            else{
                System.out.println("Invalid choice!");
            }
        }
    }

    // login function that uses Login method which was created earlier in the JLSecurity class.
    public static boolean login(JLSecurity jlSecurity) {
        Scanner reader=new Scanner(System.in);
        System.out.println("Enter your Username: ");
        String username = reader.nextLine();
        System.out.println("Enter your Password: ");
        String password = reader.nextLine();
        return jlSecurity.Login(username, password);
    }
     // CreateNewUser function that uses CreateUser method which was created earlier in the JLSecurity class.
    public static void CreateNewUser(JLSecurity jlSecurity) {
        Scanner reader=new Scanner(System.in);
        System.out.println("Enter the Username: ");
        String username = reader.nextLine();
        System.out.println("Enter the Password: ");
        String password = reader.nextLine();
        jlSecurity.CreateUser(username, password);
    }

    
}
