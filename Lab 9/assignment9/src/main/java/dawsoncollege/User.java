package dawsoncollege;

public class User implements IUser{
    //All the fields
    private String username ;
    private long failedLoginCount;
    private byte[] salt;
    private byte[] hash;

    //Constructor
    public User(String username,long failedLoginCount, byte[] salt,byte[] hash) {
        this.username = username;
        this.salt = salt;
        this.failedLoginCount = failedLoginCount;
        this.hash = hash;
    }

    //Implemented methods from IUser interface.
    @Override
    public byte[] getSalt() {
        return this.salt;
    }

    @Override
    public String getUser() {
        return this.username;
    }

    @Override
    public byte[] getHash() {
        return this.hash;
    }

    @Override
    public long getFailedLoginCount() {
        return this.failedLoginCount;
    }

    @Override
    public void setFailedLoginCount(long count) {
        this.failedLoginCount = count;
    }
    
}
