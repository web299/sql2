package dawsoncollege;
//All the import statements
import java.security.*;
import java.security.spec.InvalidKeySpecException;
import java.sql.*;
import java.util.*;
import javax.crypto.*;
import javax.crypto.spec.PBEKeySpec;

public class JLSecurity implements IJLSecurity{
    //All the fields
    private String username;
    private String password;
    private Connection conn;

    //Constructor
    public JLSecurity(String username, String password) throws SQLException {
        this.username = username;
        this.password = password;
        //A connection is established using the username and password.
        this.conn = DriverManager.getConnection("jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca", this.username, this.password);

    }
    
    //Close() closes the connection. 
    public void closeConnection() throws SQLException{
        if( this.conn!= null || !this.conn.isClosed()){
            this.conn.close();
        }
    }

    // An IUser object is created by retrieving the data(columns) based on the username. The data is passed as the parameters of 
    //User's constructor to make a User object.
    private IUser getUser(String userName){
        IUser user = null;
        // All the columns are retrieved based on the userid
        try(PreparedStatement stmt = conn.prepareStatement("SELECT * FROM JLUSERS WHERE userid = ? ")){
            stmt.setString(1, userName);
            ResultSet result = stmt.executeQuery();
            while(result.next()){
                user = new User(userName,result.getLong("failedLoginCount"), result.getBytes("salt"),result.getBytes("hash"));
            }

        }catch(SQLException e){
            e.printStackTrace();
        }
        return user;
    }

    // updateDB() sends an User(IUser) object to the database. If the user's userid is not in the database, the user gets added.
    // If the id already exists, that row gets updated based on the new data(In this case, the loginFailedCount becomes 0 again).
    //NOTE:add_user procedure calls another procedure in its exception block.
    private void updateDB(IUser user){
        try(CallableStatement stmt = conn.prepareCall("{ call add_user(?, ?, ?, ?) }")){
            stmt.setString(1, user.getUser());
            stmt.setLong(2, user.getFailedLoginCount());
            stmt.setBytes(3, user.getSalt());
            stmt.setBytes(4, user.getHash());
            stmt.execute();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    //The following method creates a random salt.
    private static byte[] createSalt(){
        SecureRandom sr=null;
        try {
            sr = SecureRandom.getInstance("SHA1PRNG");
           
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        byte[] salt = new byte[16];
        sr.nextBytes(salt);
        return salt;
    }

    //The following method creates a hash code based on the given password and salt.
    private static byte[] createHash(char[] pwd,byte[] salt,int iterationCount,int length){
        byte[] hash=null;
        SecretKeyFactory skf = null;  
        PBEKeySpec spec = new PBEKeySpec(pwd, salt, iterationCount,length);
        try {
            skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        try {
            hash = skf.generateSecret(spec).getEncoded();
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        return hash;
    }

    //The following method converts the given password which was given as a String into a char[] where each index represents a letter
    //of the password. It is created because PBEKeySpec's constructor takes a char[] for password and not a String.
    private static char[] convertPassword(String password){
        //Transforming the password to a char[]
        char[] pwd = new char[password.length()];
        for(int i = 0; i<password.length(); i++){
            pwd[i] = password.charAt(i);
        }
        return pwd;
    }

    @Override
    public void CreateUser(String user, String password) {
        //We convert the String password into a char[] password so that we can use it in createHash() function.
        char[] pwd=convertPassword(password);
        //We store the salt returned by createSalt() function into a variable.
        byte[] salt=createSalt();
        //Using the password and salt we have, we call the createHash() method so that it can make a hash for us.
        byte[] hash=createHash(pwd, salt, 500, 64*8);
        
        //A user is created based on the given information.
        IUser myUser=new User(user, 0,salt, hash);
        //User either gets added to the database or its loginFailedCount gets updated.
        updateDB(myUser);
    }
    //The following method checks to see if the two provided hash codes are actually the same or not. It is created in order to deny or
    //grant access when the user tries to login.
     private static boolean checkHash(byte[] hash,byte[] hashToCheck){
        boolean equal = Arrays.equals(hash, hashToCheck);
        return equal;
    }
    //Login() function gets the user which is already in the database using the 
    //provided username and then checks if the user can login or not based on the password.
    @Override
    public boolean Login(String user, String password) {
        //Usign getUser(), we retrieve the corresponding user in the databse with the username provided as the parameter.
        IUser myUser = getUser(user);
        if(myUser == null){
            return false;
        }

        char[] pwd = convertPassword(password);
        //The below line makes hash that will be later used to see if it is equal to the hash which is already stored in the myUser object.
        byte[] checkHash=createHash(pwd, myUser.getSalt(), 500, myUser.getHash().length*8);
       

        //compare to see if the hash generated and the hash from the databse are the same
        //If true meaning that the password is correct
        boolean equal = checkHash(myUser.getHash(),checkHash);
        if(equal == true){
            myUser.setFailedLoginCount(0);
            updateDB(myUser);
        }else{
            long failed = myUser.getFailedLoginCount();
            failed ++; 
            myUser.setFailedLoginCount(failed);
            updateDB(myUser);
        }
        return equal;
    }

}
