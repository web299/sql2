DROP TABLE jlusers;

CREATE TABLE jlusers(
    userid varchar2(2000) PRIMARY KEY,
    failedLoginCount number(3,0),
    salt RAW(16), 
    hash RAW(64)
);
/ 

CREATE OR REPLACE PROCEDURE update_jlusers(user_id varchar2, failed_Login_Count number, table_salt RAW, table_hash RAW) AS
BEGIN
    UPDATE jlusers SET
            failedLoginCount=failed_Login_Count,
            salt=table_salt,
            hash=table_hash
        WHERE
            userid = user_id;
END;
/
CREATE OR REPLACE PROCEDURE add_user(user_id varchar2, failed_Login_Count number, table_salt RAW, table_hash RAW) AS
BEGIN
    INSERT INTO jlusers VALUES (user_id, failed_Login_Count , table_salt, table_hash );
EXCEPTION
-- If the userid already exists, we just update its other columns.
    WHEN dup_val_on_index THEN
        update_jlusers(user_id , failed_Login_Count , table_salt , table_hash );
END;
