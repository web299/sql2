package ca.dawsoncollege.assignment7;

import java.sql.*;

public class DawsonCourse {
    private String courseNumber;
    private String courseName;
    private String courseDescription;
    private int classHours;
    private int labHours;
    private int homeworkHours;
    private Education education_type;
    private TermSeason termID;

    public DawsonCourse(String courseNumber, String courseName, String courseDescription, int classHours, int labHours,
            int homeworkHours, Education education_type, TermSeason termID) {
        this.courseNumber = courseNumber;
        this.courseName = courseName;
        this.courseDescription = courseDescription;
        this.classHours = classHours;
        this.labHours = labHours;
        this.homeworkHours = homeworkHours;
        this.education_type = education_type;
        this.termID = termID;
    }

    public String getCourseNumber() {
        return courseNumber;
    }

    public String getCourseName() {
        return courseName;
    }

    public String getCourseDescription() {
        return courseDescription;
    }

    public int getClassHours() {
        return classHours;
    }

    public int getLabHours() {
        return labHours;
    }

    public int getHomeworkHours() {
        return homeworkHours;
    }

    public Education getEducation_type() {
        return education_type;
    }

    public TermSeason getTermID() {
        return termID;
    }
    public void addToDatabase(Connection conn){
        
        String sql="INSERT INTO dawson_courses VALUES(?,?,?,?,?,?,?,?)";
        try(PreparedStatement stmt=conn.prepareStatement(sql)) {
            stmt.setString(1, this.courseNumber);
            stmt.setString(2, this.courseName);
            stmt.setString(3, this.courseDescription);
            stmt.setInt(4, this.classHours);
            stmt.setInt(5, labHours);
            stmt.setInt(6, this.homeworkHours);
            stmt.setString(7, this.education_type.getEducation_type_id());
            stmt.setInt(8, this.termID.getTermID());
            int result = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "DawsonCourse [courseNumber=" + courseNumber + ", courseName=" + courseName + ", courseDescription="
                + courseDescription + ", classHours=" + classHours + ", labHours=" + labHours + ", homeworkHours="
                + homeworkHours + ", education_type=" + education_type + ", termID=" + termID + "]";
    }

}