package ca.dawsoncollege.assignment7;

import java.sql.*;

public class TermSeason {
    private int termID;
    private Season season;

    public TermSeason(int termID, Season season) {
        this.termID = termID;
        this.season = season;
    }

    public int getTermID() {
        return termID;
    }

    public Season getSeason() {
        return season;
    }
    public void addToDatabase(Connection conn){
        try(PreparedStatement stmt = conn.prepareStatement("INSERT INTO term_seasons VALUES(?,?)")){
            stmt.setInt(1, this.termID);
            stmt.setString(2, this.season.getSeasonID());
            int result=stmt.executeUpdate(); 
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "TermSeason [termID=" + termID + ", season=" + season + "]";
    }

}