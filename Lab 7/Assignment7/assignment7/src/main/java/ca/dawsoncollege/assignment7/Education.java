package ca.dawsoncollege.assignment7;

import java.sql.*;

public class Education {
    private String education_type_id;
    private String educationType;

    public Education(String education_type_id, String educationType) {
        this.education_type_id = education_type_id;
        this.educationType = educationType;
    }

    public String getEducation_type_id() {
        return education_type_id;
    }

    public String getEducationType() {
        return educationType;
    }

    public void addToDatabase(Connection conn) {
        try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO educations VALUES(?,?)")) {
            stmt.setString(1, this.education_type_id);
            stmt.setString(2, this.educationType);
            int result = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Education [education_type_id=" + education_type_id + ", educationType=" + educationType + "]";
    }

}
