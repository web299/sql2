package ca.dawsoncollege.assignment7;

import java.sql.*;

public class Season {
    private String seasonID;
    private String seasonName;

    public Season(String seasonID, String seasonName) {
        this.seasonID = seasonID;
        this.seasonName = seasonName;
    }

    public String getSeasonID() {
        return seasonID;
    }

    public String getSeasonName() {
        return seasonName;
    }

    public void addToDatabase(Connection conn) {
        try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO seasons VALUES(?,?)")) {
            stmt.setString(1, this.seasonID);
            stmt.setString(2, this.seasonName);
            int result = stmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String toString() {
        return "Season [seasonID=" + seasonID + ", seasonName=" + seasonName + "]";
    }
}
